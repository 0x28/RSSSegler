//
//  RSSSeglerTests.swift
//  RSSSeglerTests
//
//  Created by Michael on 04.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import XCTest
@testable import RSSSegler

class RSSSeglerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFeedNetworking() {
        let timeout = TimeInterval(30) //seconds
        
        let links = [
            // atom feeds
            "https://www.heise.de/newsticker/heise-atom.xml",
            "www.heise.de/newsticker/heise-atom.xml",
            "heise.de/newsticker/heise-atom.xml",
            "https://rss.golem.de/rss.php?feed=ATOM1.0",
            "https://gitlab.com/0x28/RSSSegler/commits/master?format=atom",
            "https://ccc.de/de/rss/updates.xml",
            // rss feeds
            "https://netzpolitik.org/feed",
            "https://xkcd.com/rss.xml",
            // big feeds
            "http://rss.cnn.com/rss/cnn_topstories.rss",
            "https://lorem-rss.herokuapp.com/feed?unit=second&interval=1"
        ]
        
        for link in links {
            let expect = expectation(description: "load feeds async")
            FeedNetworking.loadFeed(from: link) {
                feed, error in
                
                XCTAssertNil(error)
                
                guard let feed = feed else {
                    XCTFail()
                    return
                }
                
                print("name = \(feed.name)")
                print("desc = \(feed.desc)")
                    
                XCTAssertNotEqual(feed.name, "")
                expect.fulfill()
            }
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testFeedNetworkingError() {
        let timeout = TimeInterval(30) //seconds
        
        let links = [
            "https://this.url.is.invalid.asdf",
            "https://another.invalid.url.123",
            "https://www.google.de",
            "file://this.url.is.invalid.asdf"
        ]
        
        for link in links {
            let expect = expectation(description: "load invalid feeds async")
            FeedNetworking.loadFeed(from: link) {
                feed, error in
                
                if let error = error as NSError? {
                    print("link = \(link), error = \(error)")
                } else {
                    XCTFail()
                }
                
                XCTAssertNil(feed)
                
                expect.fulfill()
            }
        }
        
        waitForExpectations(timeout: timeout, handler: nil)

    }

    /**
     This tests checks the CoreData functionality. 
     WARNING: all existing data in the database will be removed!
     */
    func testPersistence () {
        let savedRootGroup = FeedPersistence.createRootGroup()
        let savedFeed = Feed(name: "testfeed", link: "some.valid.link")
        let savedGroup = FeedGroup(name: "folder1")
        
        XCTAssertEqual(savedRootGroup.identifier, "__ID_RSSSegler_2017")
        XCTAssertEqual(savedRootGroup.name, "RSS")
        
        savedRootGroup.add(component: savedFeed)
        savedRootGroup.add(component: savedGroup)
        
        savedRootGroup.save()
        FeedPersistence.commitChanges()
        
        let restoredRootGroup = FeedPersistence.createRootGroup()
        
        XCTAssertEqual(savedRootGroup.name, restoredRootGroup.name)
        XCTAssertEqual(savedRootGroup.identifier, restoredRootGroup.identifier)
        
        XCTAssertTrue(savedRootGroup.components.count == restoredRootGroup.components.count)
        
        for (saved, restored) in zip(savedRootGroup.components, restoredRootGroup.components) {
            XCTAssertTrue(saved.identifier == restored.identifier)
            XCTAssertTrue(saved.desc == restored.desc)
            XCTAssertTrue(saved.name == restored.name)
            XCTAssertTrue(saved.newCount == restored.newCount)
        }
        
        savedRootGroup.delete()
        FeedPersistence.commitChanges()
    }
    
    func testArticleFilter () {
        let art1 = Article(identifier: "1",
                           title: "A cool new Projekt",
                           desc: "Bla bla bla",
                           publicationDate: Date.distantFuture,
                           link: "link1")
        
        let art2 = Article(identifier: "2",
                           title: "A bad old Projekt",
                           desc: "Blu blu blu",
                           publicationDate: Date.distantPast,
                           link: "link2")
        
        let art3 = Article(identifier: "3",
                           title: "New JavaScript features",
                           desc: "...",
                           publicationDate: Date.distantPast,
                           link: "link3")
        
        let filter1 = FeedFilter(name: "i dont like JavaScript")
        filter1.add(keyword: "javascript")
        
        
        XCTAssertEqual(filter1.apply(to: [art1]).count, 1)
        XCTAssertEqual(filter1.apply(to: [art2]).count, 1)
        XCTAssertEqual(filter1.apply(to: [art3]).count, 0)
        XCTAssertEqual(filter1.apply(to: [art1, art2, art3]).count, 2)
        
        let filter2 = FeedFilter(name: "only new stuff, no features")
        filter2.add(keyword: "old")
        filter2.add(keyword: "features")
        
        XCTAssertEqual(filter2.apply(to: []).count, 0)
        XCTAssertEqual(filter2.apply(to: [art1, art2, art3]).count, 1)
        XCTAssertEqual(filter2.apply(to: [art1, art1, art1]).count, 3)
        
        let filter3 = FeedFilter(name: "creates a filter and sets no keywords")
        
        XCTAssertEqual(filter3.apply(to: []).count, 0)
        XCTAssertEqual(filter3.apply(to: [art1]).count, 1)
        XCTAssertEqual(filter3.apply(to: [art1, art2]).count, 2)
        XCTAssertEqual(filter3.apply(to: [art1, art2, art3]).count, 3)
        
        let filter4 = FeedFilter(name: "no bla and no blu")
        filter4.add(keyword: "BLA")
        filter4.add(keyword: "BLU")
        
        XCTAssertEqual(filter4.apply(to: [art1, art2, art3]).count, 1)
    }
}
