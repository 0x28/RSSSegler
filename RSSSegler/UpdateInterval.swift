//
//  UpdateInterval.swift
//  RSSSegler
//
//  Created by Michael on 16.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation

enum UpdateInterval: Int {
    case min1 = 1
    case min2 = 2
    case min3 = 3
    case min5 = 5
    case min10 = 10
    case min15 = 15
    case min30 = 30
    case hour1 = 60

    func name() -> String {
        switch self {
        case .min1:
            return "1 Minute"
        case .min2:
            return "2 Minutes"
        case .min3:
            return "3 Minutes"
        case .min5:
            return "5 Minutes"
        case .min10:
            return "10 Minutes"
        case .min15:
            return "15 Minutes"
        case .min30:
            return "30 Minutes"
        case .hour1:
            return "1 Hour"
        }
    }

    func seconds() -> Int {
        return self.rawValue * 60
    }

    static let values = [min1, min2, min3, min5, min10, min15, min30, hour1]

    static var count: Int {
        return values.count
    }
}
