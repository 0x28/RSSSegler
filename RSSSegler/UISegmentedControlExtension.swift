//
//  UISegmentedControlExtension.swift
//  RSSSegler
//
//  Created by Michael on 20.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation
import UIKit.UISegmentedControl

extension UISegmentedControl {
    func selectedType(is type: NewFeedViewController.SegmentedType) -> Bool {
        return self.selectedSegmentIndex == type.rawValue
    }
}
