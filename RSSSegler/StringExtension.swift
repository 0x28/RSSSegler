//
//  StringExtension.swift
//  RSSSegler
//
//  Created by Michael on 20.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation

extension String {
    private var descLimit: Int {
        return 50
    }

    func ellipsis() -> String {
        if self.characters.count > descLimit {
            return String(format: "%@...", self.substring(to: self.index(self.startIndex, offsetBy: descLimit)))
        }
        return self
    }
}
