//
//  FeedViewController.swift
//  RSSSegler
//
//  Created by Michael on 27.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import UIKit

class FeedViewController: UITableViewController {
    var feed: Feed!

    private let dateFormatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension

        navigationItem.title = feed.name

        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        self.refreshControl?.addTarget(self, action: #selector(updateArticleTable), for: UIControlEvents.valueChanged)

        refreshControl?.beginRefreshing()
        updateArticleTable()
    }

    func updateArticleTable() {
        feed.update { [weak self] _ in
            DispatchQueue.main.async {
                self?.refreshControl?.endRefreshing()
                self?.tableView.reloadData()
            }
        }

    }

    /**
     This function handles the swipe right gesture to remove articles from the table.
     */
    override func tableView(_ tableView: UITableView,
                            editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let deleteAction = UITableViewRowAction(style: .destructive, title: "delete") { [weak self] _, _ in
            self?.feed.remove(articleAt: indexPath.row)
            tableView.reloadData()
        }
        deleteAction.backgroundColor = .red

        return [deleteAction]
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feed.count
    }

    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "feed_article",
                                                 for: indexPath)
        guard let articleCell = cell as? ArticleTableViewCell else {
            return cell
        }

        let article = feed.getArticles()[indexPath.row]

        articleCell.articleName = article.title
        articleCell.date = dateFormatter.string(from: article.publicationDate)
        articleCell.author = article.author
        articleCell.readArticle = article.read

        return articleCell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedArticle = feed.getArticles()[indexPath.row]
        selectedArticle.read = true

        if let articleViewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "article_show") as? ArticleViewController {
            articleViewController.article = selectedArticle
            navigationController?.pushViewController(articleViewController, animated: true)
        }

        tableView.reloadData()
    }

    /**
     Action when the ArticleViewController unwinds. The given article will be removed
     */
    @IBAction func unwindDeleteArticle(segue: UIStoryboardSegue) {
        if let articleViewController = segue.source as? ArticleViewController {
            if let article = articleViewController.article {
                feed.remove(article: article)
                tableView.reloadData()
            }
        }
    }
}
