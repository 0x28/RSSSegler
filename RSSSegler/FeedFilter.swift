//
//  Filter.swift
//  RSSSegler
//
//  Created by Michael on 06.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation

class FeedFilter: FeedAction {
    override func apply(to articles: [Article]) -> [Article] {
        let nonLetters = CharacterSet.letters.inverted

        return articles.filter {
            var words = Set($0.desc.components(separatedBy: nonLetters) +
                            $0.title.components(separatedBy: nonLetters))

            // remove 'empty' words and lowercase the remaining ones
            words = Set(words.filter { !$0.isEmpty }.map { $0.lowercased() })

            // words ∩ keywords = ∅ -> article contains no keywords
            return words.intersection(keywords).isEmpty
        }
    }
}
