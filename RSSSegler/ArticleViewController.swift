//
//  ArticleViewController.swift
//  RSSSegler
//
//  Created by Michael on 02.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import UIKit
import WebKit

class ArticleViewController: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBAction func openExternally(_ sender: UIBarButtonItem) {
        if let contentURL = URL(string: article?.link ?? "") {
            UIApplication.shared.open(contentURL, options: [:], completionHandler: nil)
        }
    }

    var article: Article?
    private let dateFormatter = DateFormatter()

    override func viewDidLoad() {
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let article = article {
            dateLabel.text = dateFormatter.string(from: article.publicationDate)
            navigationItem.title = article.title
        } else {
            dateLabel.text = "<no date>"
            navigationItem.title = "<no title>"
        }

        navigationItem.backBarButtonItem?.title = "Back"

        webView = WKWebView()
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.allowsBackForwardNavigationGestures = true

        stackView.addArrangedSubview(webView)
        stackView.addConstraint(NSLayoutConstraint(item: stackView,
                                                   attribute: .width,
                                                   relatedBy: .equal,
                                                   toItem: webView,
                                                   attribute: .width,
                                                   multiplier: 1,
                                                   constant: 0))

        webView.scrollView.bounces = false
        if let contentURL = URL(string: article?.link ?? "") {
            spinner.isHidden = false
            spinner.startAnimating()
            webView.load(URLRequest(url: contentURL))
        }

        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        spinner.isHidden = false
        spinner.startAnimating()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        spinner.isHidden = true
        spinner.stopAnimating()
    }

    deinit {
        webView.stopLoading()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
