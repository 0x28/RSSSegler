//
//  Feed.swift
//  RSSSegler
//
//  Created by Michael on 20.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation
import CoreData
import UIKit.UIImage

class Feed: FeedComponent {
    var identifier: String = UUID().uuidString

    var name: String
    var desc: String = ""
    var link: String
    private(set) var actions: [FeedAction] = []

    var newCount: Int {
        return articles.reduce(0) {
            $0 + ($1.read ? 0 : 1)
        }
    }
    var icon: UIImage = #imageLiteral(resourceName: "Broadcast")
    var updateInterval: UpdateInterval {
        didSet {
            updateTimer?.invalidate()
            self.updateTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(updateInterval.seconds()),
                                                    repeats: true,
                                                    block: { [weak self] _ in self?.update(callback: nil) })
        }
    }

    private var updateTimer: Timer?

    var count: Int {
        return articles.count
    }

    private var articles: [Article] = []
    private(set) var deletedArticlesIDs: [String] = []

    init(name: String, link: String, updateInterval: UpdateInterval = .min5) {
        self.name = name
        self.link = link
        self.updateInterval = updateInterval

        self.updateTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(updateInterval.seconds()),
                                                repeats: true,
                                                block: { [weak self] _ in self?.update(callback: nil) })
    }

    private static func union(old: [Article], new: [Article]) -> [Article] {
        // prepend the new articles that are not part of the old articles
        return new.filter {
            !old.contains($0)
        } + old
    }

    func update(callback: (([Article]) -> Void)?) {
        FeedNetworking.loadArticles(of: self) {
            [weak self] in

            if $1 == nil {
                var updatedArticles = $0
                // filter out deleted articles
                updatedArticles = updatedArticles.filter {
                    if let deletedIDs = self?.deletedArticlesIDs {
                        return !deletedIDs.contains($0.identifier)
                    }
                    // when in doubt keep the article
                    return true
                }

                for action in self?.actions ?? [] {
                    updatedArticles = action.apply(to: updatedArticles)
                }

                self?.articles = Feed.union(old: self?.articles ?? [], new: updatedArticles)
                self?.articles.sort {
                    $0.publicationDate > $1.publicationDate
                }
            }
            callback?(self?.articles ?? [])
        }
    }

    func delete() {
        FeedPersistence.remove(feed: self)
    }

    func save() {
        _ = FeedPersistence.save(feed: self)
    }

    func remove(article: Article) {
        deletedArticlesIDs.append(article.identifier)
        if let index = articles.index(of: article) {
            articles.remove(at: index)
        }
        article.delete()
    }

    func remove(articleAt index: Int) {
        deletedArticlesIDs.append(articles[index].identifier)
        articles.remove(at: index).delete()
    }

    func getArticles() -> [Article] {
        return articles
    }

    func add(article: Article) {
        articles.append(article)
    }

    func add(deletedArticle identifier: String) {
        deletedArticlesIDs.append(identifier)
    }

    func remove(action: FeedAction) {
        if let index = actions.index(of: action) {
            actions.remove(at: index)
        }
    }

    func add(action: FeedAction) {
        if !actions.contains(where: { $0.identifier == action.identifier }) {
            actions.append(action)
        }
    }
}
