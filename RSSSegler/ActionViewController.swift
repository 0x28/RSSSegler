//
//  ActionViewController.swift
//  RSSSegler
//
//  Created by Michael on 07.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import UIKit

class ActionViewController: UITableViewController, UIGestureRecognizerDelegate {

    @IBOutlet var rowPressRecognizer: UILongPressGestureRecognizer!
    struct Constants {
        static let sections = [ "Notifications", "Filter" ]
        static let sectionHeight = [ CGFloat(40), CGFloat(40) ]
    }

    enum ActionType: Int {
        case notification = 0
        case filter = 1
    }

    var feeds: [Feed] = []

    private var filter: Set<FeedFilter> = []
    private var notifications: Set<FeedNotification> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        for feed in feeds {
            for action in feed.actions {
                if let feedFilter = action as? FeedFilter {
                    filter.insert(feedFilter)
                } else if let notification = action as? FeedNotification {
                    notifications.insert(notification)
                }
            }
        }

        if feeds.count == 0 {
            navigationItem.rightBarButtonItem?.isEnabled = false
        }

        rowPressRecognizer.addTarget(self, action: #selector(handlePressOnRow(_:)))
        rowPressRecognizer.delegate = self
        tableView.addGestureRecognizer(rowPressRecognizer)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    /**
     This function handles the segue to view the existing action.
    */
    func handlePressOnRow(_ sender: UILongPressGestureRecognizer) {
        let point = sender.location(in: self.tableView)

        if let indexPath = tableView.indexPathForRow(at: point) {
            if sender.state == .began {
                let action: FeedAction

                if indexPath.section == ActionType.filter.rawValue {
                    action = filter[indexPath.row]
                } else {
                    action = notifications[indexPath.row]
                }

                // sender is an action -> view this action
                performSegue(withIdentifier: "add_action", sender: action)
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return Constants.sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case ActionType.filter.rawValue:
            return filter.count
        case ActionType.notification.rawValue:
            return notifications.count
        default:
            return 0
        }
    }

    /**
     This function handles the swipe right gesture to remove rows from the table.
     */
    override func tableView(_ tableView: UITableView,
                            editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let deleteAction = UITableViewRowAction(style: .destructive, title: "delete") { [weak self] _, _ in
            switch indexPath.section {
            case ActionType.filter.rawValue:
                if let startIndex = self?.filter.startIndex,
                    let removeIndex = self?.filter.index(startIndex, offsetBy: indexPath.row) {
                    self?.filter.remove(at: removeIndex).delete()
                }
            case ActionType.notification.rawValue:
                if let startIndex = self?.notifications.startIndex,
                    let removeIndex = self?.notifications.index(startIndex, offsetBy: indexPath.row) {
                    self?.notifications.remove(at: removeIndex).delete()
                }
            default:
                break
            }

            tableView.reloadSections([indexPath.section], with: UITableViewRowAnimation.none)
        }
        deleteAction.backgroundColor = .red

        return [deleteAction]
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "action_cell", for: indexPath)

        switch indexPath.section {
        case ActionType.filter.rawValue:
            cell.textLabel?.text = filter[indexPath.row].name
        case ActionType.notification.rawValue:
            cell.textLabel?.text = notifications[indexPath.row].name
        default:
            cell.textLabel?.text = ""
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Constants.sections[section]
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.sectionHeight[section]
    }

    // MARK: - Navigation

    @IBAction func unwindFromNewAction(segue: UIStoryboardSegue) {
        if let newActionViewController = segue.source as? NewActionViewController {
            if let feedFilter = newActionViewController.createdAction as? FeedFilter {
                filter.insert(feedFilter)
            } else if let feedNotification = newActionViewController.createdAction as? FeedNotification {
                notifications.insert(feedNotification)
            }

            tableView.reloadData()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? NewActionViewController,
            segue.identifier == "add_action" {
            destination.allFeeds = feeds

            if let action = sender as? FeedAction {
                destination.createdAction = action
                destination.viewing = true
            }
        }
    }

}
