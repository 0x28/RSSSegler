//
//  FeedProtocol.swift
//  RSSSegler
//
//  Created by Michael on 04.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation
import UIKit.UIImage

protocol FeedComponent {
    /**
     Identifier of the component. Used by CoreData.
    */
    var identifier: String { get }

    /**
     Name of the component. Will be shown in the UI.
    */
    var name: String { get set }

    /**
     Description of the component. Will be shown in the UI.
     */
    var desc: String { get set }

    /**
     Number of unread articles of the component. Calculated recursively for groups.
     Will be shown in the UI.
     */
    var newCount: Int { get }

    /**
     Icon of the feed or group. Defaults to a generic RSS/Folder icon. 
     Some feeds bring symbols with them, which changes the default.
     Will be shown in the UI.
     */
    var icon: UIImage { get set }

    /**
     Updates the feed/group. Works recursively for groups.
     */
    func update(callback: (([Article]) -> Void)?)

    /**
     Deletes the feed/group. All feeds below a group will also be deleted.
     */
    func delete()

    /**
     Save the feed/group. Use CoreData to save the component in the database.
     */
    func save()
}
