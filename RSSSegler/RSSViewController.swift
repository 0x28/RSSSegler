//
//  ViewController.swift
//  RSSSegler
//
//  Created by Michael on 04.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import UIKit

class RSSViewController: UITableViewController, UIGestureRecognizerDelegate {
    @IBOutlet var rowPressRecognizer: UILongPressGestureRecognizer!

    /**
     Contains the elements of the current hierarchy.
    */
    var currentGroup = FeedGroup(name: "RSS")

    /**
     This variable contains the row height, when RSSViewController first loads.
     It should contain the same value as in the storyboard.
    */
    private var rowHeight: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        if self == rootController() {
            // this is the root controller
            currentGroup = FeedPersistence.createRootGroup()

            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(appSaveState),
                                                   name: Notification.Name.UIApplicationWillTerminate,
                                                   object: nil)

            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(appSaveState),
                                                   name: Notification.Name.UIApplicationDidEnterBackground,
                                                   object: nil)
        }

        navigationItem.title = currentGroup.name
        currentGroup.update { [weak self] _ in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }

        self.refreshControl?.addTarget(self, action: #selector(updateFeedTable), for: UIControlEvents.valueChanged)
        rowHeight = tableView.rowHeight

        rowPressRecognizer.addTarget(self, action: #selector(handlePressOnRow(_:)))
        rowPressRecognizer.delegate = self
        tableView.addGestureRecognizer(rowPressRecognizer)
    }

    /**
     Save the state of the current group persistently. Will only have effect in the
     root controller.
    */
    @objc private func appSaveState() {
        // check for root controller defensively.
        if self == rootController() {
            currentGroup.save()
            FeedPersistence.commitChanges()
        }
    }

    private func rootController() -> RSSViewController? {
        return navigationController?.viewControllers[0] as? RSSViewController
    }

    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return currentGroup.count
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    /**
     This function handles the swipe right gesture to remove rows from the table.
    */
    override func tableView(_ tableView: UITableView,
                            editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let deleteAction = UITableViewRowAction(style: .destructive, title: "delete") { [weak self] _, _ in
                self?.deleteFeed(forRowAt: indexPath)
        }
        deleteAction.backgroundColor = .red

        return [deleteAction]
    }

    /**
     Every row can be edited.
    */
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    /**
     Removes the feed at the given indexPath and reloads the data.
     - parameters:
        - indexPath: The row that should be deleted.
    */
    func deleteFeed(forRowAt indexPath: IndexPath) {
        currentGroup.remove(at: indexPath.row)
        self.tableView.reloadData()
    }

    /**
     Updates every feed in the current hierarchy and below. 
     Stops the refreshing "spinner".
     Table data will also be reloaded.
     Will be called when the table is "pulled down".
     - parameters:
        - refreshControl: The spinner
    */
    func updateFeedTable(refreshControl: UIRefreshControl) {
        currentGroup.update { [weak self] _ in
            DispatchQueue.main.async {
                self?.refreshControl?.endRefreshing()
                self?.tableView.reloadData()
            }
        }
    }

    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rss_feed",
                                                 for: indexPath)
        guard let feedCell = cell as? FeedTableViewCell else {
            return cell
        }

        let feed = currentGroup[indexPath.row]

        feedCell.feedName = feed.name
        feedCell.newCount = feed.newCount
        feedCell.feedDesc = feed.desc.ellipsis()
        feedCell.iconView.image = feed.icon

        return feedCell
    }

    /**
     Tapping on a row calls this function. This results in a segue and shows a feed or changes the hierarchy.
    */
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch currentGroup[indexPath.row] {
        case let feed as Feed:
            if let feedViewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "feed_articles") as? FeedViewController {
                feedViewController.feed = feed
                navigationController?.pushViewController(feedViewController, animated: true)
            }
            break
        case let group as FeedGroup:
            if let rssViewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "main_rss") as? RSSViewController {
                rssViewController.currentGroup = group
                navigationController?.pushViewController(rssViewController, animated: true)
            }
        default:
            break
        }
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }

    /**
     The height of the rows depends on the screen height.
    */
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screenHeight = UIScreen.main.bounds.height
        /// Can be seen as the number of rows on the screen.
        let scalingFactor: CGFloat = 8

        return max(screenHeight / scalingFactor, rowHeight)
    }

    /**
     Action when the NewFeedViewController unwinds. The new feed/group will be inserted into the current hierarchy.
    */
    @IBAction func unwindFromAdd(segue: UIStoryboardSegue) {
        if let newFeedViewController = segue.source as? NewFeedViewController {
            if !newFeedViewController.modification, let component = newFeedViewController.newComponent {
                currentGroup.add(component: component)
                component.update { [weak self] _ in
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            } else {
                tableView.reloadData()
            }
        }
    }

    /**
     Handles the segues to a different hierarchy or to a feed.
    */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else {
            return
        }

        switch identifier {
        case "add_dialog":
            if let component = sender as? FeedComponent,
                let destination = segue.destination as? NewFeedViewController {
                // an existing component will be changed
                destination.newComponent = component
            }
        case "show_actions":
            if let destination = segue.destination as? ActionViewController {
                // all feeds
                destination.feeds = rootController()?.currentGroup.everyFeedBelow() ?? []
            }
        default:
            break
        }
    }

    @IBAction func handlePressOnRow(_ sender: UILongPressGestureRecognizer) {
        let point = sender.location(in: self.tableView)

        if let indexPath = tableView.indexPathForRow(at: point) {
            if sender.state == .began {
                performSegue(withIdentifier: "add_dialog", sender: currentGroup[indexPath.row])
            }
        }
    }
}
