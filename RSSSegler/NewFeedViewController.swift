//
//  NewFeedViewController.swift
//  RSSSegler
//
//  Created by Michael on 16.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import UIKit

class NewFeedViewController: UIViewController,
                             UIPickerViewDataSource,
                             UIPickerViewDelegate {

    @IBOutlet weak var minuteSelection: UIPickerView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var urlTextField: UITextField! {
        didSet {
            let activity = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            activity.color = UIColor.black
            urlTextField.rightView = activity
            urlTextField.rightViewMode = .always
            urlTextField.layoutSubviews()
        }
    }
    @IBOutlet weak var updateLabel: UILabel!
    @IBOutlet weak var typeSelection: UISegmentedControl!

    struct Constants {
        static let titleFeed = "Add Feed"
        static let titleGroup = "Add Directory"
    }

    enum SegmentedType: Int {
        case feed = 0
        case group = 1
    }

    var newComponent: FeedComponent?
    var modification = false

    override func viewDidLoad() {
        super.viewDidLoad()
        minuteSelection.dataSource = self
        minuteSelection.delegate = self

        // add dialog is used to change a feed
        if let component = newComponent {
            nameTextField.text = component.name
            if let feed = component as? Feed {
                urlTextField.text = feed.link
                minuteSelection.selectRow(UpdateInterval.values.index(of: feed.updateInterval) ??
                                            SegmentedType.feed.rawValue,
                                          inComponent: 0,
                                          animated: false)
            }
            switchType(to: component is FeedGroup ? .group : .feed)
            typeSelection.isEnabled = false
            urlTextField.isEnabled = false
            modification = true
        }

        // look for taps to dismiss keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
    }

    func hideKeyboard() {
        view.endEditing(true)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return UpdateInterval.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return UpdateInterval.values[row].name()
    }

    @IBAction func switchType(_ sender: UISegmentedControl) {
        switchType(to: SegmentedType(rawValue: sender.selectedSegmentIndex) ?? SegmentedType.feed)
    }

    /**
     Changes the UI to reflect the given type. For groups only the name can be set.
    */
    func switchType(to type: SegmentedType) {
        typeSelection.selectedSegmentIndex = type.rawValue
        switch type {
        case .feed:
            self.title = Constants.titleFeed
            minuteSelection.isHidden = false
            urlTextField.isHidden = false
            updateLabel.isHidden = false
        case .group:
            self.title = Constants.titleGroup
            minuteSelection.isHidden = true
            urlTextField.isHidden = true
            updateLabel.isHidden = true
        }
    }

    @IBAction func finishAdd(_ sender: UIBarButtonItem) {
        if modification || typeSelection.selectedType(is: SegmentedType.group) {
            performSegue(withIdentifier: "finish_add", sender: newComponent)
        } else if let url = urlTextField.text, url != "" {
            downloadFeed(url: urlTextField.text ?? "") { [weak self] feed in
                self?.newComponent = feed
                self?.performSegue(withIdentifier: "finish_add", sender: feed)
            }
        }
    }

    private func downloadFeed(url: String,
                              callback: @escaping (Feed?) -> Void) {
        guard let spinner = urlTextField.rightView as? UIActivityIndicatorView else {
            return
        }

        spinner.startAnimating()

        FeedNetworking.loadFeed(from: url) { [weak self] feed, _ in
            DispatchQueue.main.async {
                guard let feed = feed else {
                    if let textField = self?.urlTextField {
                        self?.amber(textField: textField)
                    }
                    spinner.stopAnimating()
                    return
                }
                // set the update interval
                if let row = self?.minuteSelection.selectedRow(inComponent: 0) {
                    feed.updateInterval = UpdateInterval.values[row]
                }

                // empty name-field -> use downloaded name
                if let givenName = self?.nameTextField.text, givenName != "" {
                    feed.name = givenName
                } else {
                    self?.nameTextField.text = feed.name
                    self?.nameTextField.setNeedsDisplay()
                }

                spinner.stopAnimating()
                callback(feed)
            }
        }
    }

    private func change(component: inout FeedComponent) {
        if let name = nameTextField.text {
            component.name = name
        }
        if let feed = component as? Feed {
            feed.updateInterval = UpdateInterval.values[minuteSelection.selectedRow(inComponent: 0)]
        }
    }

    /**
     This function highlights invalid text fields.
    */
    private func amber(textField: UITextField) {
        let borderWidth: CGFloat = 1
        let cornerRadius: CGFloat = 4

        textField.layer.cornerRadius = cornerRadius
        textField.layer.borderWidth = borderWidth
        textField.layer.borderColor = UIColor.red.cgColor
    }

    // MARK: - Navigation

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch identifier {
        case "finish_add":
            // stop the segue when the input fields are invalid
            if nameTextField.text == nil || nameTextField.text == "" {
                amber(textField: nameTextField)
                return false
            }
            if typeSelection.selectedType(is: SegmentedType.feed) &&
                (urlTextField.text == nil || urlTextField.text == "") {
                amber(textField: urlTextField)
                return false
            }
            return true
        default:
            return false
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else {
            return
        }

        switch identifier {
        case "finish_add":
            // change existing components
            if var component = newComponent, modification {
                change(component: &component)
            } else {
                newComponent = newComponent ?? FeedGroup(name: nameTextField.text ?? "<no name>")
            }
            break
        default:
            break
        }
    }
}
