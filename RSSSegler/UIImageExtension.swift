//
//  UIImageExtension.swift
//  RSSSegler
//
//  Created by Michael on 20.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation
import UIKit.UIImage

extension UIImage {
    func isEqual (image: UIImage) -> Bool {
        guard let selfData = UIImagePNGRepresentation(self),
            let otherData = UIImagePNGRepresentation(image) else {
                return false
        }

        return selfData == otherData
    }
}
