//
//  FeedPersistence.swift
//  RSSSegler
//
//  Created by Michael on 04.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation
import CoreData
import UIKit.UIImage

class FeedPersistence {
    private struct Constants {
        static let rootID = "__ID_RSSSegler_2017"
        static let rootName = "RSS"
    }

    /**
     Search for the entity with the identifier id. To help the type inference
     use this function like in the following example:
     ```
        let entity: EntityType? = search(by: "...")
     ```

     T: Type of the result. T also determines the name of the entity.

     - parameter id: identifier of the entity in the database.
     - returns: Entity if found in the database, nil otherwise.
     
    */
    private static func search<T: NSManagedObject>(by identifier: String) -> T? {
        let entityName = String(describing: T.self)
        let request: NSFetchRequest<T> = NSFetchRequest<T>(entityName: entityName)

        // for consistency unsafed changes will also be fetched (default is true)
        request.includesPendingChanges = true

        request.predicate = NSPredicate(format: "identifier = %@", identifier)

        guard let result = try? AppDelegate.context.fetch(request) else {
            return nil
        }

        switch result.count {
        case 0:
            return nil
        case 1:
            return result[0]
        default:
            fatalError("database consistency error: mutiple objects with the same identifier (= \(identifier))")
        }
    }

    /**
     Save a feed in the database and return its stored
     counterpart. If a feed with the same identifier is
     already in the database, change the existing one.
     Articles/Actions of this feed will also be saved. The list of
     deleted article IDs will be saved.
     
     - parameter feed: the feed that should be stored
     - returns: the CoreData feed
     
    */
    static func save(feed: Feed) -> StoredFeed {
        let storedFeed: StoredFeed = search(by: feed.identifier) ??
            StoredFeed(context: AppDelegate.context)

        storedFeed.identifier = feed.identifier
        storedFeed.desc = feed.desc
        storedFeed.link = feed.link
        storedFeed.name = feed.name
        storedFeed.updateInterval = Int64(feed.updateInterval.rawValue)
        if !feed.icon.isEqual(image: #imageLiteral(resourceName: "Broadcast")), let data = UIImagePNGRepresentation(feed.icon) as NSData? {
            storedFeed.icon = data
        }

        for article in feed.getArticles() {
            storedFeed.addToArticles(save(article: article))
        }

        for deletedID in feed.deletedArticlesIDs {
            let deletedArticle: StoredDeletedArticle = search(by: deletedID) ??
                StoredDeletedArticle(context: AppDelegate.context)

            deletedArticle.identifier = deletedID
            storedFeed.addToDeletedArticles(deletedArticle)
        }

        for action in feed.actions {
            let storedAction = save(action: action)
            storedFeed.addToActions(storedAction)
            storedAction.addToFeeds(storedFeed)
        }

        return storedFeed
    }

    /**
     Save an action in the database and return its stored
     counterpart. If an action with the same identifier is
     already in the database, change the existing one.

     - parameter action: the action that should be stored
     - returns: the CoreData action

     */
    static func save(action: FeedAction) -> StoredAction {
        let storedAction: StoredAction = search(by: action.identifier) ??
            StoredAction(context: AppDelegate.context)

        storedAction.name = action.name
        storedAction.identifier = action.identifier
        storedAction.type = String(describing: type(of: action).self)

        for keyword in action.keywords {
            let keywordIdentifier = "\(action.identifier)_\(keyword)"

            let storedKeyword: StoredKeyword = search(by: keywordIdentifier) ??
                StoredKeyword(context: AppDelegate.context)

            storedKeyword.identifier = keywordIdentifier
            storedKeyword.value = keyword

            storedAction.addToKeywords(storedKeyword)
        }

        return storedAction
    }

    /**
     Save an article in the database and return its stored
     counterpart. If an article with the same identifier is
     already in the database, change the existing one.
     
     - parameter article: the article that should be stored
     - returns: the CoreData article
     
     */
    static func save(article: Article) -> StoredArticle {
        let storedArticle: StoredArticle = search(by: article.identifier) ??
            StoredArticle(context: AppDelegate.context)

        storedArticle.desc = article.desc
        storedArticle.identifier = article.identifier
        storedArticle.link = article.link
        storedArticle.publicationDate = article.publicationDate as NSDate
        storedArticle.title = article.title
        storedArticle.author = article.author
        storedArticle.read = article.read

        return storedArticle
    }

    /**
     Save a group in the database and return its stored
     counterpart. If a group with the same identifier is
     already in the database, change the existing one. The
     components of the group will also be saved.
     
     - parameter feedGroup: the group that should be stored
     - returns: the CoreData group
     
     */
    static func save(feedGroup: FeedGroup) -> StoredFeedGroup {
        let storedFeedGroup: StoredFeedGroup = search(by: feedGroup.identifier) ??
            StoredFeedGroup(context: AppDelegate.context)

        storedFeedGroup.identifier = feedGroup.identifier
        storedFeedGroup.name = feedGroup.name
        storedFeedGroup.desc = feedGroup.desc

        for component in feedGroup.components {
            if let feed = component as? Feed {
                storedFeedGroup.addToFeeds(save(feed: feed))
            } else if let group = component as? FeedGroup {
                storedFeedGroup.addToGroups(save(feedGroup: group))
            }
        }

        return storedFeedGroup
    }

    /**
     Remove a feed in the database if it exists. Also removes all
     articles and deleted IDs of that feed.
     
     - parameter feed: the feed that should be removed from the database.
     
     */
    static func remove(feed: Feed) {
        guard let storedFeed: StoredFeed = search(by: feed.identifier) else {
            // feed is not in database -> done
            return
        }

        for action in feed.actions where action.feeds.count == 1 {
            // this is the last feed of the given action
            // actions can't exist without a feed
            remove(action: action)
        }

        // CoreDatas cascade removes articles and deleted IDs.

        AppDelegate.context.delete(storedFeed)
    }

    /**
     Remove an action in the database if it exists.

     - parameter action: the action that should be removed from the database.

     */
    static func remove(action: FeedAction) {
        guard let storedAction: StoredAction = search(by: action.identifier) else {
            return
        }

        // CoreDatas cascade removes the keywords but not the feeds!

        AppDelegate.context.delete(storedAction)
    }

    /**
     Remove an article in the database if it exists.
     
     - parameter article: the article that should be removed from the database.
     
     */
    static func remove(article: Article) {
        guard let storedArticle: StoredArticle = search(by: article.identifier) else {
            return
        }

        AppDelegate.context.delete(storedArticle)
    }

    /**
     Remove a group in the database if it exists. Also removes all feeds and groups
     below that group.
     
     - parameter feedGroup: the group that should be removed from the database.
     
     */
    static func remove(feedGroup: FeedGroup) {
        guard let storedFeedGroup: StoredFeedGroup = search(by: feedGroup.identifier) else {
            return
        }

        // CoreDatas cascade removes feeds and groups in this group.

        AppDelegate.context.delete(storedFeedGroup)
    }

    /**
     Restore a group from the database. Recursively restored feed and groups
     in that group.
     
     - parameter storedFeedGroup: the group that should be restored from the database.
     - returns: the restored feed group
     
    */
    private static func restore(storedFeedGroup: StoredFeedGroup) -> FeedGroup {
        let feedGroup = FeedGroup(name: storedFeedGroup.name!)

        feedGroup.identifier = storedFeedGroup.identifier!

        // desc is optional -> default value of FeedGroup will be used
        if let desc = storedFeedGroup.desc {
            feedGroup.desc = desc
        }

        for storedFeed in storedFeedGroup.feeds ?? [] {
            if let storedFeed = storedFeed as? StoredFeed {
                feedGroup.add(component: restore(storedFeed: storedFeed))
            }
        }

        for storedGroup in storedFeedGroup.groups ?? [] {
            if let storedGroup = storedGroup as? StoredFeedGroup {
                feedGroup.add(component: restore(storedFeedGroup: storedGroup))
            }
        }

        return feedGroup
    }

    /**
     Restore an article from the database if it exists.
     
     - parameter storedArticle: the article that should be restored from the database.
     - returns: the restored article
     
     */
    private static func restore(storedArticle: StoredArticle) -> Article {
        let article = Article(identifier: storedArticle.identifier!,
                              title: storedArticle.title!,
                              desc: storedArticle.desc!,
                              publicationDate: storedArticle.publicationDate! as Date,
                              link: storedArticle.link!,
                              author: storedArticle.author!)

        article.read = storedArticle.read

        return article
    }

    /**
     Restore a feed from the database. Recursively restore articles of that feed.
     
     - parameter storedFeed: the feed that should be restored from the database.
     - returns: the restored feed
     
     */
    private static func restore(storedFeed: StoredFeed) -> Feed {
        let feed = Feed(name: storedFeed.name!,
                        link: storedFeed.link!)

        feed.identifier = storedFeed.identifier!
        if let desc = storedFeed.desc {
            feed.desc = desc
        }

        feed.updateInterval = UpdateInterval(rawValue: Int(storedFeed.updateInterval)) ?? UpdateInterval.min5

        for storedArticle in storedFeed.articles ?? [] {
            if let storedArticle = storedArticle as? StoredArticle {
                feed.add(article: restore(storedArticle: storedArticle))
            }
        }

        for storedDeletedArticle in storedFeed.deletedArticles ?? [] {
            if let storedDeletedArticle = storedDeletedArticle as? StoredDeletedArticle,
                let id = storedDeletedArticle.identifier {
                feed.add(deletedArticle: id)
            }
        }

        for storedAction in storedFeed.actions ?? [] {
            if let storedAction = storedAction as? StoredAction {
                let action = restore(storedAction: storedAction)

                feed.add(action: action)
                action.add(feed: feed)
            }
        }

        if let data = storedFeed.icon as Data? {
            if let icon = UIImage(data: data) {
                feed.icon = icon
            }
        }

        return feed
    }

    // multiple restores of an action should always return the same action
    private static var restoredActions: [String : FeedAction] = [:]

    /**
     Restore an action from the database.

     - parameter storedAction: the action that should be restored from the database.
     - returns: the restored action

     */
    private static func restore(storedAction: StoredAction) -> FeedAction {
        var action: FeedAction

        if let action = restoredActions[storedAction.identifier ?? ""] {
            return action
        }

        if storedAction.type! == "FeedFilter" {
            action = FeedFilter(name: storedAction.name!, identifier: storedAction.identifier!)
        } else {
            action = FeedNotification(name: storedAction.name!, identifier: storedAction.identifier!)
        }

        for storedKeyword in storedAction.keywords ?? [] {
            if let storedKeyword = storedKeyword as? StoredKeyword {
                action.add(keyword: storedKeyword.value!)
            }
        }

        restoredActions[storedAction.identifier!] = action

        return action
    }

    /**
     Get or create the root feed group in the database.
     
     - returns: the feed group representing the root of all feeds.
    */
    static func createRootGroup() -> FeedGroup {
        var rootGroup: StoredFeedGroup? = search(by: Constants.rootID)

        if rootGroup == nil {
            rootGroup = StoredFeedGroup(context: AppDelegate.context)
            rootGroup?.identifier = Constants.rootID
            rootGroup?.name = Constants.rootName
        }

        return restore(storedFeedGroup: rootGroup!)
    }

    /**
     Make the changed entities persistent.
    */
    static func commitChanges() {
        AppDelegate.saveData()
    }
}
