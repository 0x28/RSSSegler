//
//  FeedActionSetExtension.swift
//  RSSSegler
//
//  Created by Michael on 20.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation

extension Set where Element: FeedAction {
    subscript(index: Int) -> FeedAction {
        return self[self.index(self.startIndex, offsetBy: index)]
    }
}
