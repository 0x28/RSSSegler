//
//  ArticleTableViewCell.swift
//  RSSSegler
//
//  Created by Michael on 27.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
    @IBOutlet weak var articleNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!

    struct Constants {
        static let oldColor = UIColor.lightGray
        static let newColor = UIColor.black
    }

    var articleName = "" {
        didSet {
            articleNameLabel.text = articleName
            setNeedsDisplay()
        }
    }

    var date = "" {
        didSet {
            dateLabel.text = date
            setNeedsDisplay()
        }
    }

    var author = "" {
        didSet {
            authorLabel.text = author
            setNeedsDisplay()
        }
    }

    var readArticle = true {
        didSet {
            if readArticle {
                articleNameLabel.textColor = Constants.oldColor
                authorLabel.textColor = Constants.oldColor
                dateLabel.textColor = Constants.oldColor
            } else {
                articleNameLabel.textColor = Constants.newColor
                authorLabel.textColor = Constants.newColor
                dateLabel.textColor = Constants.newColor
            }
            setNeedsDisplay()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        articleNameLabel.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
