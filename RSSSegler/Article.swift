//
//  Article.swift
//  RSSSegler
//
//  Created by Michael on 27.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation

class Article: Equatable, Hashable {
    let identifier: String
    let title: String
    let desc: String
    let publicationDate: Date
    let link: String
    let author: String
    var read: Bool

    var hashValue: Int {
        return identifier.hashValue
    }

    init(identifier: String,
         title: String,
         desc: String,
         publicationDate: Date,
         link: String,
         author: String) {
        self.identifier = identifier
        self.title = title
        self.desc = desc
        self.publicationDate = publicationDate
        self.link = link
        self.read = false
        self.author = author
    }

    func delete () {
        FeedPersistence.remove(article: self)
    }
}

func == (left: Article, right: Article) -> Bool {
    return left.identifier == right.identifier
}
