//
//  NewActionViewController.swift
//  RSSSegler
//
//  Created by Michael on 07.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import UIKit

class NewActionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,
                               UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var feedKeywordTableView: UITableView!
    @IBOutlet weak var typeSelection: UISegmentedControl!

    var feedPicker = UIPickerView()

    struct Constants {
        static let sections = [ "Feeds", "Keywords" ]
        static let sectionHeight = [ CGFloat(40), CGFloat(40) ]
        static let sectionAddPrompt = [ "Add feed ...", "Add keyword ..." ]
        static let namePlaceholder = [
            ActionType.notification: "Notification name",
            ActionType.filter: "Filter name"
        ]
        static let colorMercury = UIColor(red: 0xE2, green: 0xE2, blue: 0xE2, alpha: 0x00)
    }

    enum Section: Int {
        case feed = 0
        case keyword = 1
    }

    enum ActionType {
        case notification
        case filter
    }

    var allFeeds: [Feed] = []

    private var actionType = ActionType.notification
    private var selectedKeywords: [String] = []
    private var selectedFeeds: [Feed] = []

    var createdAction: FeedAction?
    var viewing = false

    @IBAction func actionTypeChanged(_ sender: UISegmentedControl) {
        switch actionType {
        case .notification:
            actionType = .filter
        case .filter:
            actionType = .notification
        }

        nameLabel.placeholder = Constants.namePlaceholder[actionType]
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        feedKeywordTableView.dataSource = self
        feedKeywordTableView.delegate = self

        feedPicker.dataSource = self
        feedPicker.delegate = self
        feedPicker.backgroundColor = Constants.colorMercury

        // look for taps to dismiss input
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideInput))
        view.addGestureRecognizer(tap)

        // while viewing actions all input fields are disabled
        if viewing {
            nameLabel.text = createdAction?.name
            nameLabel.isEnabled = false
            typeSelection.selectedSegmentIndex = createdAction is FeedFilter ? 1 : 0
            typeSelection.isEnabled = false
            selectedFeeds = createdAction?.feeds ?? []
            selectedKeywords = Array(createdAction?.keywords ?? [])
            navigationItem.title = createdAction?.name
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView,
                   editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "delete") { [weak self] _, _ in
            switch indexPath.section {
            case Section.feed.rawValue:
                self?.selectedFeeds.remove(at: indexPath.row)
            case Section.keyword.rawValue:
                self?.selectedKeywords.remove(at: indexPath.row)
            default:
                break
            }

            tableView.reloadSections([indexPath.section], with: UITableViewRowAnimation.none)
        }
        deleteAction.backgroundColor = .red

        return [deleteAction]
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if viewing {
            return false
        }

        // one can't remove the input field cell ...
        if indexPath.section == Section.feed.rawValue && indexPath.row == selectedFeeds.count {
            return false
        }

        // one can't remove the input field cell ...
        if indexPath.section == Section.keyword.rawValue && indexPath.row == selectedKeywords.count {
            return false
        }

        return true
    }

    @IBAction func newAction(_ sender: UIBarButtonItem) {
        if !shouldPerformSegue(withIdentifier: "new_action", sender: self) {
            return
        }

        switch actionType {
        case .filter:
            createdAction = FeedFilter(name: nameLabel.text ?? "<no name>")
        case .notification:
            createdAction = FeedNotification(name: nameLabel.text ?? "<no name>")
        }

        for keyword in selectedKeywords {
            createdAction?.add(keyword: keyword)
        }

        for feed in selectedFeeds {
            if let action = createdAction {
                feed.add(action: action)
            }
        }

        createdAction?.add(feeds: selectedFeeds)

        performSegue(withIdentifier: "new_action", sender: self)
    }

    // Text field stuff

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        acceptKeyword(textField: textField)

        return false
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        acceptKeyword(textField: textField)
    }

    func acceptKeyword(textField: UITextField) {
        if textField.inputView == feedPicker {
            return
        }

        if let keyword = textField.text, keyword != "" {
            selectedKeywords.append(keyword.lowercased())
        }
        textField.text = nil
        view.resignFirstResponder()
        feedKeywordTableView.reloadData()
    }

    @objc private func hideInput() {
        view.endEditing(true)
    }

    // Picker view stuff

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return allFeeds.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return allFeeds[row].name
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedFeeds.append(allFeeds[row])
        view.resignFirstResponder()
        feedKeywordTableView.reloadData()
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return Constants.sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0

        switch section {
        case Section.feed.rawValue:
            numberOfRows = selectedFeeds.count + 1
        case Section.keyword.rawValue:
            numberOfRows = selectedKeywords.count + 1
        default:
            break
        }

        return numberOfRows
    }

    /**
     The last row in a given section is an input field. The other rows are ether keywords or feeds.
    */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // doesn't need to be optional because the following if sets cell regardless of conditions
        var cell: UITableViewCell

        if indexPath.section == Section.feed.rawValue && indexPath.row == selectedFeeds.count ||
            indexPath.section == Section.keyword.rawValue && indexPath.row == selectedKeywords.count {
            // this is a input text cell
            cell = tableView.dequeueReusableCell(withIdentifier: "action_config_input", for: indexPath)

            if let cell = cell as? InputTableViewCell {
                cell.inputTextField.placeholder = viewing ? "" : Constants.sectionAddPrompt[indexPath.section]
                cell.inputTextField.textColor = UIColor.lightGray
                cell.inputTextField.isEnabled = !viewing

                if indexPath.section == Section.feed.rawValue {
                    cell.inputTextField.inputView = feedPicker
                } else {
                    cell.inputTextField.inputView = nil
                }

                cell.inputTextField.delegate = self
            }
        } else {
            // this is a normal cell
            cell = tableView.dequeueReusableCell(withIdentifier: "action_config", for: indexPath)
            cell.textLabel?.textColor = UIColor.black

            switch indexPath.section {
            case Section.feed.rawValue:
                cell.textLabel?.text = selectedFeeds[indexPath.row].name
            case Section.keyword.rawValue:
                cell.textLabel?.text = selectedKeywords[indexPath.row]
            default:
                break
            }
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.sectionHeight[section]
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Constants.sections[section]
    }

    private func amber(textField: UITextField) {
        let borderWidth: CGFloat = 1
        let cornerRadius: CGFloat = 4

        textField.layer.cornerRadius = cornerRadius
        textField.layer.borderWidth = borderWidth
        textField.layer.borderColor = UIColor.red.cgColor
    }

    // MARK: - Navigation

    /**
     Show a simple message to the user.
    */
    private func warnUser(title: String, message: String) {
        let alert = UIAlertController()
        alert.title = title
        alert.message = message
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch identifier {
        case "new_action":
            if nameLabel.text == "" || nameLabel.text == nil {
                amber(textField: nameLabel)
                return false
            }

            if selectedFeeds.count == 0 {
                warnUser(title: "Invalid number of feeds",
                         message: "At least one feed must be specified.")
                return false
            }

            if selectedKeywords.count == 0 {
                warnUser(title: "Invalid number of keywords",
                         message: "At least one keyword must be specified")
                return false
            }
            return true
        default:
            return false
        }
    }

}
