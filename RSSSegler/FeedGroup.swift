//
//  FeedGroup.swift
//  RSSSegler
//
//  Created by Michael on 07.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation
import UIKit.UIImage

class FeedGroup: FeedComponent {
    var identifier: String = UUID().uuidString

    var name: String
    var desc: String = ""
    var newCount: Int {
            return components.reduce(0) { $0 + $1.newCount }
    }
    var icon: UIImage = #imageLiteral(resourceName: "Folder")

    private(set) var components: [FeedComponent] = []

    var count: Int {
        return components.count
    }

    subscript(index: Int) -> FeedComponent {
        get {
            return components[index]
        }
    }

    init(name: String) {
        self.name = name
    }

    func update(callback: (([Article]) -> Void)?) {
        _ = components.map {
            $0.update(callback: nil)
        }

        if let callback = callback {
            callback([])
        }
    }

    func delete() {
        FeedPersistence.remove(feedGroup: self)
        components = []
    }

    func save() {
        _ = FeedPersistence.save(feedGroup: self)
    }

    func add(component: FeedComponent) {
        components.append(component)
        // feeds are always on the top -> true = $0 first, false = $1 first
        // if the type is the same sort by the name attribute
        components.sort(by: {
            switch ($0, $1) {
            case is (Feed, Feed):
                fallthrough
            case is (FeedGroup, FeedGroup):
                return $0.name < $1.name
            case is (Feed, FeedComponent):
                return true
            case is (FeedGroup, FeedComponent):
                return false
            default:
                return false

            }
        })
    }

    func remove(at index: Int) {
        components[index].delete()
        components.remove(at: index)
    }

    /**
     Returns every feed in this group and the groups below.
    */
    func everyFeedBelow() -> [Feed] {
        var feeds: [Feed] = []

        for component in components {
            if let feed = component as? Feed {
                feeds.append(feed)
            } else if let group = component as? FeedGroup {
                feeds.append(contentsOf: group.everyFeedBelow())
            }
        }

        return feeds
    }
}
