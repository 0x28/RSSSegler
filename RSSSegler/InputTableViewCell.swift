//
//  InputTableViewCell.swift
//  RSSSegler
//
//  Created by Michael on 08.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import UIKit

class InputTableViewCell: UITableViewCell {
    @IBOutlet weak var inputTextField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
