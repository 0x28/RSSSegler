//
//  FeedTableViewCell.swift
//  RSSSegler
//
//  Created by Michael on 09.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    @IBOutlet weak var feedNameLabel: UILabel!
    @IBOutlet weak var feedDescLabel: UILabel!
    @IBOutlet weak var newCountLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!

    struct Constants {
        static let borderWidth: CGFloat = 1
        static let cornerRadius: CGFloat = 10
    }

    var feedName: String = "" {
        didSet {
            feedNameLabel?.text = feedName
            setNeedsDisplay()
        }
    }
    var feedDesc: String = "" {
        didSet {
            feedDescLabel?.text = feedDesc
            setNeedsDisplay()
        }
    }

    var newCount: Int = 0 {
        didSet {
            // empty counts are not shown
            newCountLabel?.text = newCount > 0 ? "(\(newCount))" : ""
            setNeedsDisplay()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        iconView.layer.allowsEdgeAntialiasing = true
        iconView.layer.borderWidth = Constants.borderWidth
        iconView.layer.cornerRadius = Constants.cornerRadius
        iconView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
