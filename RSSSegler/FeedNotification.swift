//
//  Notification.swift
//  RSSSegler
//
//  Created by Michael on 07.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation
import UserNotifications

class FeedNotification: FeedAction {
    private let delay = 10

    private func notify(title: String, body: String) {
        let nowTrigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval(delay), repeats: false)

        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()

        let request = UNNotificationRequest(identifier: "feedNotify", content: content, trigger: nowTrigger)

        UNUserNotificationCenter.current().add(request) {
            if let error = $0 {
                print(error)
            }
        }
    }

    override func apply(to articles: [Article]) -> [Article] {
        let nonLetters = CharacterSet.letters.inverted

        for article in articles {
            var words = Set(article.desc.components(separatedBy: nonLetters) +
                            article.title.components(separatedBy: nonLetters))

            // remove 'empty' words and lowercase the remaining ones
            words = Set(words.filter { !$0.isEmpty }.map { $0.lowercased() })

            // at least one keyword was found
            if words.intersection(keywords).count > 0 {
                notify(title: article.title, body: article.desc)
            }
        }

        return articles
    }
}
