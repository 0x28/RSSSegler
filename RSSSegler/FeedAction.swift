//
//  FeedAction.swift
//  RSSSegler
//
//  Created by Michael on 06.06.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation

typealias Keyword = String

class FeedAction: Equatable, Hashable {
    private(set) var identifier: String = UUID().uuidString
    let name: String

    private(set) var feeds: [Feed] = []

    private(set) var keywords: Set<Keyword> = []

    init(name: String) {
        self.name = name
    }

    init(name: String, identifier: String) {
        self.name = name
        self.identifier = identifier
    }

    var hashValue: Int {
        return identifier.hashValue
    }

    /**
     Add a new keyword from the action.
     
     - parameter keyword: the keyword that should be added
    */
    func add(keyword: Keyword) {
        keywords.insert(keyword.lowercased())
    }

    /**
     Remove a keyword from the action, if the actions 
     contains that keyword.
     
     - parameter keyword: the keyword that should be removed
    */
    func remove(keyword: Keyword) {
        keywords.remove(keyword.lowercased())
    }

    /**
     Check articles for keywords and activate the action
     if a keyword is found.
     
     - parameter articles: the articles that should be checked
     - returns: the modified articles (result depends on the child class)
    */
    func apply(to articles: [Article]) -> [Article] {
        return articles
    }

    func delete() {
        for feed in feeds {
            feed.remove(action: self)
        }
        FeedPersistence.remove(action: self)
    }

    func add(feed: Feed) {
        if !feeds.contains { $0.identifier == feed.identifier } {
            feeds.append(feed)
        }
    }

    func add(feeds: [Feed]) {
        for feed in feeds {
            add(feed: feed)
        }
    }

    static func == (lhs: FeedAction, rhs: FeedAction) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
