//
//  FeedNetworking.swift
//  RSSSegler
//
//  Created by Michael on 20.05.17.
//  Copyright © 2017 Michael. All rights reserved.
//

import Foundation
import FeedKit
import SwiftSoup

class FeedNetworking {

    static let errorDomain = "FeedNetworking"

    enum ErrorCodes: Int {
        case invalidResourceType = 10
        case resourceDoesNotExist = 20
        case invalidURL = 30
        case jsonNotSupported = 40
    }

    /**
     Checks if the resource at the given url exists. Requests are send synchronously.
     - parameters:
        - url: URL of the resource
        - timeout: give up after n seconds and return false
    */
    private static func resourceExists(_ url: URL, timeout: Double = 3.0) -> Bool {
        let semaphore = DispatchSemaphore(value: 0)
        var exists = false
        var request = URLRequest(url: url)
        request.httpMethod = "HEAD"
        request.timeoutInterval = timeout

        let task = URLSession.shared.dataTask(with: request) { _, response, _ in
            if let httpResp = response as? HTTPURLResponse {
                switch httpResp.statusCode {
                case 200:
                    exists = true
                default:
                    exists = false
                }
            }
            semaphore.signal()
        }

        task.resume()
        semaphore.wait()

        return exists
    }

    /**
     Loads the articles of a given feed.
     - parameter feed: the articles of this feed are loaded
     - parameter finished: closure that accepts a list of articles and an error. 
       This closure will be called when all articles have been downloaded. The closure will not
       be called on the main queue.
    */
    static func loadArticles(of feed: Feed,
                             finished callback: @escaping ([Article], Error?) -> Void) {
        var articles: [Article] = []

        DispatchQueue.global(qos: .userInitiated).async {

            guard let url = URL(string: feed.link), let newFeed = FeedParser(URL: url)?.parse() else {
                callback(articles, NSError(domain: errorDomain,
                                           code: ErrorCodes.invalidResourceType.rawValue,
                                           userInfo: nil))
                return
            }

            switch newFeed {
            case .rss(let rssFeed):
                for item in rssFeed.items ?? [] {
                    let newArticle = Article(identifier: item.guid?.value ?? UUID().uuidString,
                                             title: item.title ?? "<no title>",
                                             desc: item.description ?? "",
                                             publicationDate: item.pubDate ?? Date(timeIntervalSinceNow: 0),
                                             link: item.link ?? "",
                                             author: item.author ?? "")
                    articles.append(newArticle)
                }
                callback(articles, nil)
                break
            case .atom(let atomFeed):
                for entry in atomFeed.entries ?? [] {
                    let newArticle = Article(identifier: entry.id ?? UUID().uuidString,
                                             title: entry.title ?? "<no title>",
                                             desc: entry.summary?.value ?? "",
                                             publicationDate: entry.updated ?? Date(timeIntervalSinceNow: 0),
                                             link: entry.links?[0].attributes?.href ?? "",
                                             author: entry.authors?.first?.name ?? "")
                    articles.append(newArticle)
                }
                callback(articles, nil)
                break
            case .failure(let error):
                callback(articles, error)
            case .json:
                callback(articles, NSError(domain: errorDomain,
                                           code: ErrorCodes.jsonNotSupported.rawValue,
                                           userInfo: nil))
            }
        }
    }

    /**
     Load logo from a location. Loads synchronously.
     - parameter string: location of the logo.
     - returns: the UIImage of the logo. nil on error.
    */
    private static func loadLogo(string: String) -> UIImage? {
        guard let url = URL(string: string) else {
            return nil
        }

        return loadLogo(url: url)
    }

    /**
     Load logo from a url.
     - parameter url: URL of the logo. Loads synchronously.
     - returns: the UIImage of the logo. nil on error.
     */
    private static func loadLogo(url: URL) -> UIImage? {
        guard let data = try? Data(contentsOf: url) else {
            return nil
        }

        return UIImage(data: data)
    }

    /**
     Download a feed from a given URL.
     - parameters:
        - url: URL of the feed
        - retry: If the first url isn't a valid feed we retry it with feeds we find
          in the website. Retry is true for the second attempt.
        - callback: closure to call when finished. 
          parameter $0 of the closure is nil if an error occured.
          parameter $1 contains the error.
    */
    static func loadFeed(from url: URL,
                         retry: Bool = false,
                         finished callback: @escaping (Feed?, Error?) -> Void) {
        var resultFeed: Feed?

        DispatchQueue.global(qos: .userInitiated).async {
            if !resourceExists(url) {
                callback(nil, NSError(domain: errorDomain,
                                      code: ErrorCodes.resourceDoesNotExist.rawValue,
                                      userInfo: nil))
                return
            }

            guard let newFeed = FeedParser(URL: url)?.parse() else {
                callback(nil, NSError(domain: errorDomain,
                                      code: ErrorCodes.invalidResourceType.rawValue,
                                      userInfo: nil))
                return
            }

            switch newFeed {
            case .rss(let rssFeed):
                resultFeed = Feed(name : rssFeed.title ?? "", link: url.absoluteString)
                resultFeed?.desc = rssFeed.description ?? ""
                if let icon = loadLogo(string: rssFeed.image?.url ?? "") {
                    resultFeed?.icon = icon
                }
                callback(resultFeed, nil)
            case .atom(let atomFeed):
                resultFeed = Feed(name : atomFeed.title ?? "", link: url.absoluteString)
                resultFeed?.desc = atomFeed.subtitle?.value ?? ""
                resultFeed?.link = url.absoluteString
                if let icon = loadLogo(string: atomFeed.logo ?? "") {
                    resultFeed?.icon = icon
                }
                callback(resultFeed, nil)
            case .failure(let error):
                if retry {
                    callback(resultFeed, error)
                } else if let foundFeed = searchFeeds(in: url).first {
                    loadFeed(from: foundFeed, retry: true, finished: callback)
                } else {
                    callback(resultFeed, error)
                }
            case .json:
                callback(nil, NSError(domain: errorDomain,
                                      code: ErrorCodes.jsonNotSupported.rawValue,
                                      userInfo: nil))
            }
        }
    }

    /**
     Searches for feeds in a given url.
     - parameter url: a website that may contain links to feeds
     - returns: list of found feed urls
    */
    private static func searchFeeds(in url: URL) -> [URL] {
        var foundFeeds: [URL] = []

        guard let html = try? String(contentsOf: url, encoding: .ascii) else {
            return []
        }
        guard let document = try? SwiftSoup.parse(html) else {
            return []
        }

        guard let feedLinks = try? document.select("[type~=application/(rss|atom)\\+xml]") else {
            return []
        }

        for link in feedLinks {
            if let foundURL = URL(string: (try? link.attr("href")) ?? "") {
                foundFeeds.append(foundURL)
            }
        }

        return foundFeeds
    }

    /**
     Wrapper around loadFeed. Prepends "https://" if neither "https://" nor "http://" are
     at the beginning of the given string.
     - parameters:
        - string: string representing the URL
        - callback: closure to call when finished downloading
    */
    static func loadFeed(from string: String,
                         finished callback: @escaping (Feed?, Error?) -> Void) {
        var string = string
        if !string.hasPrefix("https://") && !string.hasPrefix("http://") {
            string = "https://" + string
        }
        if let url = URL(string: string) {
            loadFeed(from: url, finished: callback)
        } else {
            callback(nil, NSError(domain: errorDomain,
                                  code: ErrorCodes.invalidURL.rawValue,
                                  userInfo: nil))
        }
    }
}
